<?php
/**
 * Created by PhpStorm.
 * User: jpike
 * Date: 27/03/15
 * Time: 2:27 PM
 */

class Helper {

    /**
     * Check to see if the URI begins with admin.
     * Used for route filtering/authentication
     *
     * @return bool
     */
    public static function isAdminRequest()
    {
        return (Route::getCurrentRoute()->getPrefix() == '/admin');
    }

}