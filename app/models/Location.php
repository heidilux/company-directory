<?php
/**
 * Created by PhpStorm.
 * User: jpike
 * Date: 20/10/14
 * Time: 11:56 AM
 */

class Location extends Eloquent {

    public $timestamps = false;

    public function employees()
    {
        return $this->hasMany('Employee', 'location_id');
    }
} 