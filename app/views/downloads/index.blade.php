@extends('layouts.master')

@section('body')

    <div class="row">
        <div class="col-lg-12 text-center">
            <h1>Employee Downloads</h1>
            <hr/>
            <p class="lead">Click on a button to download...</p>

            <table class="table table-bordered">
                <tbody>
                @foreach ($downloads as $file)
                <tr>
                    <td><a class="btn btn-primary btn-block" href="{{ route('download', [$file->filename]) }}">{{ $file->display_name }}</a></td>
                    <td class="vert-align">{{ $file->description }}</td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@stop
