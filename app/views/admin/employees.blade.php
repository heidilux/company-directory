@extends('layouts.master')
@section('body')

    <ul class="nav nav-tabs">
        <li role="presentation" class="active"><a href="#new" data-toggle="tab"><i class="fa fa-user-plus"></i> Add New Employee</a> </li>
        <li role="presentation"><a href="#edit" data-toggle="tab"><i class="fa fa-pencil"></i> Edit Employee</a> </li>
    </ul>

    <div class="tab-content">
        <div id="new" class="tab-pane active">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2 class="panel-title">Create a new employee</h2>
                </div>
                <div class="panel-body">
                    {{ Form::open(['route' => 'addEmployee', 'class' => 'form-horizontal']) }}

                    @include('admin._partials.add-employee-form')

                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary btn-block">Create</button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
        <div id="edit" class="tab-pane">
            {{ Form::open(['route' => 'chooseEmployee']) }}
            <div class="form-group">
                {{ Form::label('choose', 'Choose Employee', ['class' => 'col-sm-3 control-label']) }}
                <div class="col-sm-9">
                    {{ Form::select('choose', $employees, null, ['class' => 'form-control', 'onchange' => 'this.form.submit()']) }}
                </div>
            </div>
            {{ Form::close() }}
        </div>

    </div>

    @include('admin._partials.delete-confirmation')
@stop

@section('scripts')
    <script src="{{ asset('js/delete-confirm-modal.js') }}"></script>
@stop