@extends('layouts.master')
@section('body')

    <ul class="nav nav-tabs">
        <li role="presentation" class="active"><a href="#current" data-toggle="tab"><i class="fa fa-list"></i> Current Departments</a> </li>
        <li role="presentation"><a href="#new" data-toggle="tab"><i class="fa fa-plus"></i> Create New Department</a> </li>
    </ul>

    <div class="tab-content">
        <div id="current" class="tab-pane active">
            <div class="well">
                <ul>
                    <li>Renaming a department will rename it across everyone that has it</li>
                    <li>You cannot delete a department that is in use.  You must remove it from all employees before deleting</li>
                </ul>
            </div>
            <table class="table">
                <thead>
                <th>Name</th>
                <th></th>
                </thead>
                <tbody>
                @foreach($depts as $dept)
                    <tr>
                        <td>{{ $dept->name }}</td>
                        <td>
                            <a href="{{ route('updateDepartment', $dept->id) }}" class="btn btn-sm btn-primary">Edit Name</a>
                            {{ Form::open(['route' => ['deleteDepartment', $dept->id],'class' => 'form-table-btn']) }}
                            {{ Form::button('Delete', [
                                'class' => 'btn btn-sm btn-danger',
                                'data-toggle' => 'modal',
                                'data-target' => '#confirmDelete',
                                'data-title'  => 'Delete Department',
                                'data-message' => 'Are you sure you want to delete this department?'
                            ]) }}
                            {{ Form::close() }}
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
        <div id="new" class="tab-pane">
            {{ Form::open(['route' => 'createDepartment', 'files' => true]) }}
            <div class="col-md-10">
                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                    {{ Form::label('name', 'New Department Name') }}
                    {{ Form::text('name', null, ['class' => 'form-control']) }}
                    {{ $errors->first('name', '<span class="help-block">:message</span>') }}
                </div>
                {{ Form::submit('Create', ['class' => 'btn btn-primary']); }}
            </div>


            {{ Form::close() }}
        </div>
    </div>

    @include('admin._partials.delete-confirmation')
@stop

@section('scripts')
    <script src="{{ asset('js/delete-confirm-modal.js') }}"></script>
@stop