<?php

class DownloadsController extends \BaseController {

	/**
	 * Show the file download page
	 *
	 * @return Response
	 */
	public function index()
	{
		$downloads = Download::all();
        return View::make('downloads.index', compact('downloads'));
	}

    /**
     * Download a file
     *
     * @param $filename
     * @return mixed
     */
    public function download($filename)
    {
        $file = public_path() . "/files/" . $filename;
        $headers = ['Content-Type' =>  'application/pdf'];

        return Response::download($file, $filename, $headers);
    }

}