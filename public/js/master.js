$('#style-chooser').on('click', '.theme-choice', function(e) {
    var style = getActiveStyleSheet()
    $('.theme-choice i').remove();
    if ($('.theme-choice').is('#'+style)){
        $('#'+style+ '> a').prepend('<i class="fa fa-check"></i> ');
    }
});

$('.dropdown input, .dropdown label').click (function(e) {
    e.stopPropagation();
});

$(document).ready(function () {
    var dataTable = $('#directory-table').DataTable( {
        paging: false,
        sDom:   'ltr',
        order:  [[ 1, 'asc']]
    });

    $('#search-box').keyup(function() {
        dataTable.search(this.value).draw();
    });

    $('#login-form').on('submit', function(e) {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/login',
            data: $(this).serialize(),
            dataType: 'json'
        })
            .success(function(data) {
                if(data.success == true){
                    window.location.href = "/admin/dashboard";
                } else {
                    // Add error notice to login box...
                }

            })
            .fail(function(data) {
                console.log('FAIL -- ' + data.msg);
            });
    });

    $("#master-alert").delay(3000).fadeOut();
});