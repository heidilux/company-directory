<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmployeesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('employees', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('firstname', 32);
			$table->string('lastname', 32);
			$table->string('extension', 4)->default('-');
			$table->string('primary_email', 50)->default('-');
			$table->string('secondary_email', 50)->default('-');
			$table->string('gmail_email', 50)->default('-');
			$table->string('role', 32);
			$table->integer('location_id');
			$table->integer('dept_id');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('employees');
	}

}
