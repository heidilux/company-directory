@extends('layouts.master')
@section('body')


    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="panel-title">Edit Employee</h2>
        </div>
        <div class="panel-body">
            {{ Form::model($employee, ['route' => ['editEmployee'], 'class' => 'form-horizontal']) }}

            @include('admin._partials.edit-employee-form')

            <div class="col-sm-7 col-sm-offset-3">
                <button type="submit" class="btn btn-primary btn-block">Update</button>
            </div>

            {{ Form::close() }}
            <div class="col-sm-2">
                {{ Form::open(['route' => ['deleteEmployee', $employee->id]]) }}
                {{ Form::button('Delete', [
                    'class' => 'btn btn-primary btn-danger',
                    'data-toggle' => 'modal',
                    'data-target' => '#confirmDelete',
                    'data-title'  => 'Delete Employee',
                    'data-message' => 'Are you sure you want to delete this employee?'
                ]) }}
                {{ Form::hidden('id', $employee->id) }}
                {{ Form::close() }}
            </div>


            @include('admin._partials.delete-confirmation')
        </div>
    </div>

@stop

@section('scripts')
    <script src="{{ asset('js/delete-confirm-modal.js') }}"></script>
@stop