@extends('layouts.master')
@section('body')

    <ul class="nav nav-tabs">
        <li role="presentation" class="active"><a href="#current" data-toggle="tab"><i class="fa fa-list"></i> Current Downloads</a> </li>
        <li role="presentation"><a href="#new" data-toggle="tab"><i class="fa fa-plus"></i> Upload New File</a> </li>
    </ul>

    <div class="tab-content">
        <div id="current" class="tab-pane active">
            <table class="table">
                <thead>
                <th>Display Name</th>
                <th>Filename</th>
                <th>Description</th>
                <th></th>
                </thead>
                <tbody>
                @foreach($downloads as $file)
                    <tr>
                        <td>{{ $file->display_name }}</td>
                        <td>{{ $file->filename }}</td>
                        <td>{{ $file->description }}</td>
                        <td>
                            {{ Form::open(['route' => ['deleteFile', $file->id]]) }}
                            {{ Form::submit('Delete', ['class' => 'btn btn-sm btn-danger']) }}
                            {{ Form::close() }}
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
        <div id="new" class="tab-pane">
            {{ Form::open(['route' => 'uploadFile', 'files' => true]) }}
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('display_name') ? 'has-error' : '' }}">
                    {{ Form::label('display_name', 'Display Name') }}
                    {{ Form::text('display_name', null, ['class' => 'form-control', 'placeholder' => 'This will appear on the download button']) }}
                    {{ $errors->first('display_name', '<span class="help-block">:message</span>') }}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('upload') ? 'has-error' : '' }}">
                    {{ Form::label('upload', 'New File') }}
                    {{ Form::file('upload') }}
                    {{ $errors->first('upload', '<span class="help-block">:message</span>') }}
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group {{ $errors->has('display_name') ? 'has-error' : '' }}">
                    {{ Form::label('description', 'Short Description') }}
                    {{ Form::textarea('description', null, ['class' => 'form-control']) }}
                    {{ $errors->first('description', '<span class="help-block">:message</span>') }}
                </div>
            </div>
            <div class="col-md-2">
                {{ Form::submit('Upload File', ['class' => 'btn btn-primary']); }}
            </div>

            {{ Form::close() }}
        </div>
    </div>



@stop