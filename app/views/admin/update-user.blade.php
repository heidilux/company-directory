@extends('layouts.master')
@section('body')

    {{ Form::open() }}

    <div class="col-md-10">
        <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
            {{ Form::label('password', 'New Password') }}
            {{ Form::password('password', ['class' => 'form-control']) }}
            {{ $errors->first('password', '<span class="help-block">:message</span>') }}
        </div>
        <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
            {{ Form::label('password_confirmation', 'Confirm') }}
            {{ Form::password('password_confirmation', ['class' => 'form-control']) }}
            {{ $errors->first('password_confirmation', '<span class="help-block">:message</span>') }}
        </div>
        {{ Form::submit('Update Password', ['class' => 'btn btn-primary']) }}
    </div>

    {{ Form::close() }}


@stop

