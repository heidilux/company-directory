<?php

class Employee extends Eloquent {

    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function location() {

        return $this->belongsTo('Location');

    }

    public function department() {

        return $this->belongsTo('Department', 'dept_id');

    }

} 