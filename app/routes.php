<?php

// Front-end Pages
Route::get('/', ['as' => 'index', 'uses' => 'DirectoryController@index']);
Route::get('login', ['as' => 'login', 'uses' => 'AuthController@show']);
Route::post('login', ['as' => 'login', 'uses' => 'AuthController@login']);
Route::get('downloads', ['as' => 'downloads', 'uses' => 'DownloadsController@index']);
Route::get('download/{filename}', ['as' => 'download', 'uses' => 'DownloadsController@download']);

Route::group(['before' => 'auth'], function()
{
    Route::get('logout', ['as' => 'logout', 'uses' => 'AuthController@logout']);

    // Dashboard pages
    Route::group(['prefix' => 'admin'], function()
    {
        Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'AdminController@index']);

        // Manage Employees
        Route::get('employees', ['as' => 'manageEmployees', 'uses' => 'AdminController@manageEmployees']);
        Route::post('employee/add', ['as' => 'addEmployee', 'uses' => 'AdminController@doAddEmployee']);
        Route::post('employee/choose', ['as' => 'chooseEmployee', 'uses' => 'AdminController@doChooseEmployee']);
        Route::post('employee/edit', ['as' => 'editEmployee', 'uses' => 'AdminController@doEditEmployee']);
        Route::post('employee/delete/{id}', ['as' => 'deleteEmployee', 'uses' => 'AdminController@deleteEmployee']);

        // Manage Admin Users
        Route::get('add-user', ['as' => 'addUser', 'uses' => 'AdminController@showAddUser']);
        Route::get('users', ['as' => 'showUsers', 'uses' => 'AdminController@showUsers']);
        Route::post('users', ['as' => 'addUser', 'uses' => 'AdminController@doAddUser']);
        Route::get('update-user/{id}', ['as' => 'updateUser', 'uses' => 'AdminController@showUpdateUser']);
        Route::post('update-user/{id}', ['as' => 'updateUser', 'uses' => 'AdminController@doUpdateUser']);
        Route::post('delete-user/{id}', ['as' => 'deleteUser', 'uses' => 'AdminController@deleteUser']);

        // Manage Downloads
        Route::get('manage-downloads', ['as' => 'employeeDownloads', 'uses' => 'AdminController@manageDownloads']);
        Route::post('upload-file', ['as' => 'uploadFile', 'uses' => 'AdminController@uploadFile']);
        Route::post('delete-file/{id}', ['as' => 'deleteFile', 'uses' => 'AdminController@deleteFile']);

        // Manage Departments
        Route::get('departments', ['as' => 'manageDepartments', 'uses' => 'AdminController@manageDepartments']);
        Route::post('create-department', ['as' => 'createDepartment', 'uses' => 'AdminController@createDept']);
        Route::get('update-department/{id}', ['as' => 'updateDepartment', 'uses' => 'AdminController@showUpdateDept']);
        Route::post('update-department/{id}', ['as' => 'updateDepartment', 'uses' => 'AdminController@doUpdateDept']);
        Route::post('delete-department/{id}', ['as' => 'deleteDepartment', 'uses' => 'AdminController@deleteDept']);

        // Manage Locations
        Route::get('locations', ['as' => 'manageLocations', 'uses' => 'AdminController@manageLocations']);
        Route::post('create-location', ['as' => 'createLocation', 'uses' => 'AdminController@createLocation']);
        Route::get('update-location/{id}', ['as' => 'updateLocation', 'uses' => 'AdminController@showUpdateLocation']);
        Route::post('update-location/{id}', ['as' => 'updateLocation', 'uses' => 'AdminController@doUpdateLocation']);
        Route::post('delete-location/{id}', ['as' => 'deleteLocation', 'uses' => 'AdminController@deleteLocation']);

    });
});