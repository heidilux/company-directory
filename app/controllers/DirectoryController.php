<?php

class DirectoryController extends \BaseController {

    /**
     * Show the main index (directory listing)
     *
     * @return mixed
     */
    public function index()
	{
        return View::make('index');
	}

}