<nav id="sidebar-nav">
    <ul class="nav nav-pills nav-stacked">
        <li class="{{ HTML::activeState(['manageEmployees']) }}"><a href="{{ route('manageEmployees') }}"><i class="fa fa-users"></i> Employees</a></li>
        <li class="{{ HTML::activeState(['employeeDownloads']) }}"><a href="{{ route('employeeDownloads') }}"><i class="fa fa-download"></i> Downloads</a></li>
        <li class="nav-divider"></li>
        <li class="{{ HTML::activeState(['manageDepartments']) }}"><a href="{{ route('manageDepartments') }}"><i class="fa fa-building"></i> Departments</a></li>
        <li class="{{ HTML::activeState(['manageLocations']) }}"><a href="{{ route('manageLocations') }}"><i class="fa fa-map-marker"></i> Locations</a></li>
        <li class="{{ HTML::activeState(['showUsers']) }}"><a href="{{ route('showUsers') }}"><i class="fa fa-user"></i> Admin Users</a></li>

    </ul>
</nav>