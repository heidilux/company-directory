@extends('layouts.master')
@section('body')

    <h2>Edit Location Name</h2>
    <hr>

    {{ Form::open() }}

    <div class="col-md-10">
        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
            {{ Form::label('name', 'New name') }}
            {{ Form::text('name', $location->name, ['class' => 'form-control']) }}
            {{ $errors->first('name', '<span class="help-block">:message</span>') }}
        </div>
        {{ Form::submit('Update name', ['class' => 'btn btn-primary']) }}
    </div>

    {{ Form::close() }}


@stop

