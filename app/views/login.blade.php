@extends('layouts.master')
@section('body')
    
    {{ Form::open() }}

    <div class="col-md-6 col-md-offset-3">
        <div class="form-group {{ $errors->has('username') ? 'has-error' : '' }}">
            {{ Form::label('username', 'Username') }}
                {{ Form::text('username', null, ['class' => 'form-control']) }}
                {{ $errors->first('username', '<span class="help-block">:message</span>') }}
        </div>
        <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
            {{ Form::label('password', 'Password') }}
                {{ Form::password('password', ['class' => 'form-control']) }}
                {{ $errors->first('password', '<span class="help-block">:message</span>') }}
        </div>
        {{ Form::submit('Login', ['class' => 'btn btn-primary']) }}
    </div>


    
    {{ Form::close() }}
    
@stop