<?php

use Illuminate\Support\Facades\Validator;

class AdminController extends \BaseController {

	/**
	 * Show main dashboard page
	 *
	 * @return Response
	 */
	public function index()
	{
        return View::make('admin.index');
	}

    /**
     * Show employee management tabs
     *
     * @return mixed
     */
    public function manageEmployees()
    {
        $dept = ['' => 'Choose...'] + Department::lists('name', 'id');
        $location = ['' => 'Choose...'] + Location::lists('name', 'id');
        foreach (Employee::orderBy('lastname', 'ASC')->get() as $emp)
        {
            $fullNames[] = $emp->firstname . ' ' . $emp->lastname;
            $empIds[] = $emp->id;
        }
        $employees = array_combine($empIds, $fullNames);
        $employees = ['' => 'Choose...'] + $employees;
        return View::make('admin.employees', compact('dept', 'location', 'employees'));
    }

    /**
     * Add new employee to the DB
     *
     * @return mixed
     */
    public function doAddEmployee()
    {
        $employee = [
            'firstname' => Input::get('firstname'),
            'lastname' => Input::get('lastname'),
            'extension' => Input::get('extension'),
            'primary_email' => Input::get('primary_email'),
            'role' => Input::get('role'),
            'location_id' => Input::get('location'),
            'dept_id' => Input::get('dept')
        ];

        $rules = [
            'firstname' =>  'required',
            'lastname'  =>  'required',
            'extension' =>  'numeric',
            'primary_email' => 'email',
            'role'      =>  'required',
            'location_id'   => 'required',
            'dept_id'   => 'required'
        ];

        $messages = [
            'firstname.required' => 'Please enter a first name',
            'lastname.required' => 'Please enter a last name',
            'extension.numeric' => 'The extension can only contain numbers',
            'primary_email.email' => 'Please enter a valid email',
            'role.required'      =>  'Please enter a role',
            'location_id.required'   => 'You must choose a location',
            'dept_id.required'   => 'You must choose a department'
        ];

        $validator = Validator::make($employee, $rules, $messages);

        if ($validator->passes())
        {
            Employee::create($employee);

            return Redirect::back();
        } else {
            return Redirect::back()->withErrors($validator)->withInput();
        }

    }

    /**
     * User chose single employee from dropdown to be edited
     *
     * @return mixed
     */
    public function doChooseEmployee()
    {
        $employee = Employee::find(Input::get('choose'));
        $dept = ['' => 'Choose...'] + Department::lists('name', 'id');
        $location = ['' => 'Choose...'] + Location::lists('name', 'id');
        return View::make('admin.edit-employee', compact('dept', 'location', 'employee'));
    }

    /**
     * Update employee record in DB
     *
     * @return mixed
     */
    public function doEditEmployee()
    {
        $employee = Employee::find(Input::get('id'));
        $input = Input::all();
        array_shift($input);
        unset($input['id']);

        $employee->fill($input)->save();

        $name = $employee->firstname . ' ' . $employee->lastname;

        return Redirect::route('manageEmployees')->with('flash_message', $name . ' has been edited');
    }

    public function deleteEmployee()
    {
        $employee = Employee::find(Input::get('id'));

        $employee->delete();

        return Redirect::route('manageEmployees')
            ->with('flash_message', 'Employee ' . $employee->firstname . ' ' . $employee->lastname . ' has been deleted');
    }

    /**
     * Show form to create new admin user
     *
     * @return mixed
     */
    public function showAddUser()
    {
        return View::make('admin.register');
    }

    /**
     * Save new admin user to DB
     *
     * @return mixed
     */
    public function doAddUser()
    {

        $rules = [
            'username'  => 'required',
            'email'     => 'required|email',
            'password'  => 'required|confirmed',
            'password_confirmation' => 'required'
        ];

        $v = Validator::make(Input::all(), $rules);

        if ($v->fails())
        {
            return Redirect::back()->withErrors($v)->withInput(Input::except('password'));
        } else {
            $user = new User;
            $user->username = Input::get('username');
            $user->email = Input::get('email');
            $user->password = Hash::make(Input::get('password'));
            $user->save();

            return Redirect::route('showUsers')->with('flash_message', 'New user ' . $user->username . ' created');
        }
    }

    /**
     * Show list of current admin users
     *
     * @return mixed
     */
    public function showUsers()
    {
        $users = User::all();
        return View::make('admin.users', compact('users'));
    }

    /**
     * Delete an admin user (cannot delete admin user with id = 1)
     *
     * @param $id
     * @return mixed
     */
    public function deleteUser($id)
    {
        if ($id !== 1)
        {
            $user = User::find($id);
            $user->delete();

            return Redirect::back()->with('flash_message', 'User ' . $user->username . ' has been deleted');
        } else {
            return Redirect::back()->with('danger_flash_message', 'The admin user cannot be deleted');
        }

    }

    /**
     * Show form to change admin user password
     *
     * @param $id
     * @return mixed
     */
    public function showUpdateUser($id)
    {
        $user = User::find($id);

        return View::make('admin.update-user', compact('user'));
    }

    /**
     * Save new admin user password to DB
     *
     * @param $id
     * @return mixed
     */
    public function doUpdateUser($id)
    {
        $rules = [
            'password'  => 'required|confirmed',
            'password_confirmation' => 'required'
        ];

        $v = Validator::make(Input::all(), $rules);

        if ($v->fails())
        {
            return Redirect::back()->withErrors($v);
        } else {
            $user = User::find($id);
            $user->password = Hash::make(Input::get('password'));
            $user->save();

            return Redirect::route('showUsers')->with('flash_message', 'User ' . $user->username . ' password updated');
        }
    }

    /**
     * Show list of current available downloads
     *
     * @return mixed
     */
    public function manageDownloads()
    {
        $downloads = Download::all();
        return View::make('admin.downloads', compact('downloads'));
    }

    /**
     * Save new downloadable file to path and record in DB
     *
     * @return mixed
     */
    public function uploadFile()
    {
        if (Input::hasFile('upload'))
        {
            $filename = Input::file('upload')->getClientOriginalName();
            Input::file('upload')->move(public_path('files'), $filename);
        }

        $displayName = Input::get('display_name');
        $description = Input::get('description');

        $newFile = new Download;
        $newFile->display_name = $displayName;
        $newFile->filename = $filename;
        $newFile->description = $description;

        $newFile->save();

        return Redirect::back()->with('flash_message', 'File Uploaded');

    }

    /**
     * Delete downloadable file from path and DB
     *
     * @param $id
     * @return mixed
     */
    public function deleteFile($id)
    {
        $file = Download::find($id);
        File::delete($file->filename);
        $file->delete();

        return Redirect::back()->with('flash_message', $file->filename . ' deleted');
    }

    /**
     * Show list of employee department choices
     *
     * @return mixed
     */
    public function manageDepartments()
    {
        $depts = Department::all();

        return View::make('admin.departments', compact('depts'));
    }

    /**
     * Create new employee department choice
     *
     * @return mixed
     */
    public function createDept()
    {
        $rules = [
            'name' => 'required|alpha_spaces'
        ];

        $v = Validator::make(Input::all(), $rules);

        if ($v->fails())
        {
            return Redirect::back()->withErrors($v);
        } else {
            $dept = new Department;
            $name = Input::get('name');

            $dept->name = $name;
            $dept->save();

            return Redirect::route('manageDepartments')->with('flash_message', 'Department created');
        }
    }

    /**
     * Show form to update employee department name
     *
     * @param $id
     * @return mixed
     */
    public function showUpdateDept($id)
    {
        $dept = Department::find($id);

        return View::make('admin.edit-department', compact('dept'));
    }

    /**
     * Save updated department name to DB
     *
     * @param $id
     * @return mixed
     */
    public function doUpdateDept($id)
    {
        $rules = [
            'name' => 'required|alpha_spaces'
        ];

        $v = Validator::make(Input::all(), $rules);

        if ($v->fails())
        {
            return Redirect::back()->withErrors($v);
        } else {
            $dept = Department::find($id);
            $newName = Input::get('name');

            $dept->name = $newName;
            $dept->save();

            return Redirect::route('manageDepartments')->with('flash_message', 'Department name, ' . $dept->name . ' updated');
        }

    }

    /**
     * Delete department from DB.  Cannot delete one that's in use.
     *
     * @param $id
     * @return mixed
     */
    public function deleteDept($id)
    {
        $dept = Department::find($id);
        if (count($dept->employees()->get()->toArray()))
        {
            return Redirect::back()->with('danger_flash_message', 'You cannot delete a department that is in use.');
        }

        $dept->delete();

        return Redirect::route('manageDepartments')->with('flash_message', 'Department ' . $dept->name . ' has been deleted');
    }

    /**
     * Show list of employee locations
     *
     * @return mixed
     */
    public function manageLocations()
    {
        $locs = Location::all();

        return View::make('admin.locations', compact('locs'));
    }

    /**
     * Create new employee location
     *
     * @return mixed
     */
    public function createLocation()
    {
        $rules = [
            'name' => 'required|alpha_spaces'
        ];

        $v = Validator::make(Input::all(), $rules);

        if ($v->fails())
        {
            return Redirect::back()->withErrors($v);
        } else {
            $location = new Location;
            $name = Input::get('name');

            $location->name = $name;
            $location->save();

            return Redirect::route('manageLocations')->with('flash_message', 'Location created');
        }
    }

    /**
     * Show form to update employee location name
     *
     * @param $id
     * @return mixed
     */
    public function showUpdateLocation($id)
    {
        $location = Location::find($id);

        return View::make('admin.edit-location', compact('location'));
    }

    /**
     * Save updated employee location name
     *
     * @param $id
     * @return mixed
     */
    public function doUpdateLocation($id)
    {
        $rules = [
            'name' => 'required|alpha_spaces'
        ];

        $v = Validator::make(Input::all(), $rules);

        if ($v->fails())
        {
            return Redirect::back()->withErrors($v);
        } else {
            $location = Location::find($id);
            $newName = Input::get('name');

            $location->name = $newName;
            $location->save();

            return Redirect::route('manageLocations')->with('flash_message', 'Location name, ' . $location->name . ' updated');
        }

    }

    /**
     * Delete employee location from DB
     *
     * @param $id
     * @return mixed
     */
    public function deleteLocation($id)
    {
        $location = Location::find($id);
        if (count($location->employees()->get()->toArray()))
        {
            return Redirect::back()->with('danger_flash_message', 'You cannot delete a location that is in use.');
        }

        $location->delete();

        return Redirect::route('manageLocations')->with('flash_message', 'Location ' . $location->name . ' has been deleted');
    }

}