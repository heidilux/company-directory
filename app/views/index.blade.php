@extends('layouts.master')

@section('styles')
 <link href="{{ asset('css/jquery.dataTables.css') }}" rel="stylesheet">
@stop

@section('body')

<table id="directory-table" class="table table-striped">
    <thead>
        <th>First</th>
        <th>Last</th>
        <th>Deptartment</th>
        <th>Role</th>
        <th>Location</th>
        <th>Extension</th>
        <th>Email</th>
    </thead>
    <tbody>
        @foreach(Employee::orderBy('lastname')->get() as $employee)
        <tr>
            <td>{{ $employee->firstname }}</td>
            <td>{{ $employee->lastname }}</td>
            <td>{{ Department::find($employee->dept_id)->name }}</td>
            <td>{{ $employee->role }}</td>
            <td>{{ Location::find($employee->location_id)->name }}</td>
            <td>{{ $employee->extension }}</td>
            <td>{{ $employee->primary_email }}</td>
        </tr>
        @endforeach
    </tbody>
</table>

@stop

@section('scripts')
<script src="{{ asset('js/jquery.dataTables.js') }}"></script>
@stop