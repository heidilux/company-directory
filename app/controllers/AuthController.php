<?php

use Illuminate\Support\Facades\Validator;

class AuthController extends BaseController {

    /**
     * Show the login form
     *
     * @return mixed
     */
    public function show()
    {
        return View::make('login');
    }

    /**
     * Log the user in if credentials are good
     *
     * @return mixed
     */
    public function login()
    {
        $rules = [
            'username' => 'required',
            'password' => 'required'
        ];

        $v = Validator::make(Input::all(), $rules);

        if ($v->passes())
        {
            $username = Input::get('username');
            $password = Input::get('password');

            if (Auth::attempt(['username' => $username, 'password' => $password]))
            {
                return Redirect::intended('admin/dashboard');
            } else {
                return Redirect::back()->withInput(Input::except('password'))->with('danger_flash_message', 'Invalid Credentials');
            }
        } else {
            return Redirect::back()->withErrors($v)->withInput(Input::except('password'));
        }
    }

    /**
     * Log the user out
     * 
     * @return mixed
     */
    public function logout()
    {
        Auth::logout();

        return Redirect::route('index');
    }
}