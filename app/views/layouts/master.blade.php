<!DOCTYPE html>
<html lang="en-us" xmlns="http://www.w3.org/1999/html">

<head>
    @include('includes.head')
</head>

<body>
    @include('includes.nav')

    <div class="content">
        @if(Helper::isAdminRequest())
            <div class="container">
                <div class="row">
                    <div id="sidebar" class="col-md-3">
                        @include('includes.sidebar')
                    </div>
                    <div id="content" class="col-md-9">
                        @include('includes.flashes')
                        @yield('body')
                    </div>
                </div>
            </div>

        @else
            <div class="container">
                @include('includes.flashes')
                @yield('body')
            </div>
        @endif
    </div>

    @include('includes.footer')

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script src="{{ asset('js/styleswitcher.js') }}"></script>
<script src="{{ asset('js/master.js') }}"></script>
@yield('scripts')

</body>
</html>
