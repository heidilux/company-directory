@extends('layouts.master')
@section('body')

    <ul class="nav nav-tabs">
        <li role="presentation" class="active"><a href="#current" data-toggle="tab"><i class="fa fa-users"></i> Current Users</a> </li>
        <li role="presentation"><a href="#new" data-toggle="tab"><i class="fa fa-user-plus"></i> Create New User</a> </li>
    </ul>

    <div class="tab-content">
        <div id="current" class="tab-pane active">
            <table class="table">
                <thead>
                <th>Username</th>
                <th>Email</th>
                <th></th>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td class="{{ $user->id == 1 ? 'text-muted' : '' }}">{{ $user->username }}</td>
                        <td class="{{ $user->id == 1 ? 'text-muted' : '' }}">{{ $user->email }}</td>
                        <td>
                            @if (!($user->id == 1))
                            <a href="{{ route('updateUser', $user->id) }}"
                               class="btn btn-sm btn-primary">
                               Update Password
                            </a>
                            {{ Form::open(['route' => ['deleteUser', $user->id],'class' => 'form-table-btn']) }}
                            {{ Form::button('Delete', [
                                'class' => 'btn btn-sm btn-danger',
                                'data-toggle' => 'modal',
                                'data-target' => '#confirmDelete',
                                'data-title'  => 'Delete Admin User',
                                'data-message' => 'Are you sure you want to delete this user?'
                            ]) }}
                            {{ Form::close() }}
                            @endif
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
        <div id="new" class="tab-pane">
            {{ Form::open() }}

            <div class="col-md-10">
                <div class="form-group {{ $errors->has('username') ? 'has-error' : '' }}">
                    {{ Form::label('username', 'Username') }}
                    {{ Form::text('username', null, ['class' => 'form-control']) }}
                    {{ $errors->first('username', '<span class="help-block">:message</span>') }}
                </div>
                <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                    {{ Form::label('email', 'Email') }}
                    {{ Form::email('email', null, ['class' => 'form-control']) }}
                    {{ $errors->first('email', '<span class="help-block">:message</span>') }}
                </div>
                <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                    {{ Form::label('password', 'Password') }}
                    {{ Form::password('password', ['class' => 'form-control']) }}
                    {{ $errors->first('password', '<span class="help-block">:message</span>') }}
                </div>
                <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                    {{ Form::label('password_confirmation', 'Confirm') }}
                    {{ Form::password('password_confirmation', ['class' => 'form-control']) }}
                    {{ $errors->first('password_confirmation', '<span class="help-block">:message</span>') }}
                </div>
                {{ Form::submit('Register', ['class' => 'btn btn-primary']) }}
            </div>
            {{ Form::close() }}
        </div>
    </div>

    @include('admin._partials.delete-confirmation')
@stop

@section('scripts')
    <script src="{{ asset('js/delete-confirm-modal.js') }}"></script>
@stop