<div id="footer">
    <div class="container">
        <div class="footer-text text-muted footer-left">Copyright &copy; AMS Uniforms {{ date("Y") }}</div>
    </div>
</div>