<div class="form-group {{ $errors->has('firstname') ? 'has-error' : '' }}">
    {{ Form::label('firstname', 'First Name', ['class' => 'col-sm-3 control-label']) }}
    <div class="col-sm-9">
        {{ Form::text('firstname', null, ['class' => 'form-control']) }}
        {{ $errors->first('firstname', '<span class="help-block">:message</span>') }}
    </div>
</div>

<div class="form-group {{ $errors->has('lastname') ? 'has-error' : '' }}">
    {{ Form::label('lastname', 'Last Name', ['class' => 'col-sm-3 control-label']) }}
    <div class="col-sm-9">
        {{ Form::text('lastname', null, ['class' => 'form-control']) }}
        {{ $errors->first('lastname', '<span class="help-block">:message</span>') }}
    </div>
</div>

<div class="form-group {{ $errors->has('dept_id') ? 'has-error' : '' }}">
    {{ Form::label('dept_id', 'Department', ['class' => 'col-sm-3 control-label']) }}
    <div class="col-sm-9">
        {{ Form::select('dept_id', $dept, $employee->dept_id, ['class' => 'form-control']) }}
        {{ $errors->first('dept_id', '<span class="help-block">:message</span>') }}
    </div>
</div>

<div class="form-group {{ $errors->has('role') ? 'has-error' : '' }}">
    {{ Form::label('role', 'Role', ['class' => 'col-sm-3 control-label']) }}
    <div class="col-sm-9">
        {{ Form::text('role', null, ['class' => 'form-control']) }}
        {{ $errors->first('role', '<span class="help-block">:message</span>') }}
    </div>
</div>

<div class="form-group {{ $errors->has('location_id') ? 'has-error' : '' }}">
    {{ Form::label('location_id', 'Location', ['class' => 'col-sm-3 control-label']) }}
    <div class="col-sm-9">
        {{ Form::select('location_id', $location, $employee->location_id, ['class' => 'form-control']) }}
        {{ $errors->first('location_id', '<span class="help-block">:message</span>') }}
    </div>
</div>

<div class="form-group {{ $errors->has('extension') ? 'has-error' : '' }}">
    {{ Form::label('extension', 'Extension', ['class' => 'col-sm-3 control-label']) }}
    <div class="col-sm-9">
        {{ Form::text('extension', null, ['class' => 'form-control']) }}
        {{ $errors->first('extension', '<span class="help-block">:message</span>') }}
    </div>
</div>

<div class="form-group {{ $errors->has('primary_email') ? 'has-error' : '' }}">
    {{ Form::label('primary_email', 'Email', ['class' => 'col-sm-3 control-label']) }}
    <div class="col-sm-9">
        {{ Form::text('primary_email', null, ['class' => 'form-control', 'type' => 'email']) }}
        {{ $errors->first('primary_email', '<span class="help-block">:message</span>') }}
    </div>
</div>

{{ Form::hidden('id', $employee->id) }}