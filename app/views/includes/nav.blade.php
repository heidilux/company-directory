<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ route('index') }}">
                AMS Directory
            </a>
        </div>

        <div class="collapse navbar-collapse navbar-ex1-collapse">
            @if (Route::currentRouteName() == 'index')
            <form class="navbar-form navbar-left" role="search">
                <div class="form-group">
                    <input type="text" id="search-box" class="form-control" placeholder="Search">
                </div>
            </form>
            @endif
            <ul class="nav navbar-nav navbar-left">
                <li>
                    @if (Auth::check())
                        <a href="{{ route('dashboard') }}">Dashboard</a>
                    @endif
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{ route('downloads') }}">Employee Downloads</a> </li>
                <li>
                    @if (Auth::check())
                        <a href="{{ route('logout') }}">Logout</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                    @endif
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Choose Theme <span class="caret"></span></a>
                    <ul class="dropdown-menu" id="style-chooser">
                        <li class="theme-choice" id="superhero"><a href="#" onclick="setActiveStyleSheet('superhero'); return false;">Superhero</a></li>
                        <li class="theme-choice" id="cerulean"><a href="#" onclick="setActiveStyleSheet('cerulean'); return false;">Cerulean</a></li>
                        <li class="theme-choice" id="sandstone"><a href="#" onclick="setActiveStyleSheet('sandstone'); return false;">Sandstone</a></li>
                        <li class="theme-choice" id="slate"><a href="#" onclick="setActiveStyleSheet('slate'); return false;">Slate</a></li>
                        <li class="theme-choice" id="spacelab"><a href="#" onclick="setActiveStyleSheet('spacelab'); return false;">Spacelab</a></li>
                        <li class="theme-choice" id="amelia"><a href="#" onclick="setActiveStyleSheet('amelia'); return false;">Amelia</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>