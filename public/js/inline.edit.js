$(document).ready(function () {
    $(function() {
        $('td[contenteditable=true]').blur(function() {
            var emp_id = $(this).attr('id');
            var value = $(this).text();
            $.ajax({
                type: 'POST',
                url: '/update',
                data: {field_id: emp_id, value: value},
                dataType: 'json'
            })
                .success(function(data) {
//                    $('#edit-alert').html(data.employee + ' has been edited!').show();
//                    $("#edit-alert").delay(1000).fadeOut();
                })
                .fail(function(data) {
                    console.log('FAIL -- ' + data.msg);
                });
        });

        $('.dept-sized, .loc-sized').change(function() {
            var emp_id = $(this).closest('td').attr('id');
            var value = $(this).closest('td').find(':input').val();
            $.ajax({
                type: 'POST',
                url: '/update',
                data: {field_id: emp_id, value: value},
                dataType: 'json'
            })
                .success(function(data) {

                })
                .fail(function(data) {
                    console.log('FAIL -- ' + data.msg);
                });

        });
    });
});
