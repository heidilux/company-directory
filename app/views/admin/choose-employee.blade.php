@extends('layouts.master')
@section('body')

    {{ Form::open() }}
    <div class="form-group">
        {{ Form::label('choose', 'Choose Employee', ['class' => 'col-sm-3 control-label']) }}
        <div class="col-sm-9">
            {{ Form::select('choose', $employees, null, ['class' => 'form-control', 'onchange' => 'this.form.submit()']) }}
        </div>
    </div>
    {{ Form::close() }}

@stop