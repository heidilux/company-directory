<?php
/**
 * Created by PhpStorm.
 * User: jpike
 * Date: 20/10/14
 * Time: 11:55 AM
 */

class Department extends Eloquent {

    public function employees()
    {
        return $this->hasMany('Employee', 'dept_id');
    }
} 